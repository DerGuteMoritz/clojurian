(cond-expand
  (chicken-5
   (import (chicken load) (test)))
  (else
   (use test)))

(test-begin)
(load-relative "syntax.scm")
(load-relative "atom.scm")
(test-end)
