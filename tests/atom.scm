(cond-expand
  (chicken-5
   (import (chicken random) (test) (srfi 1) (srfi 18))
   (import (clojurian atom)))
  (else
   (use test srfi-1 srfi-18)
   (use clojurian-atom)))


(test-begin "atom")

(let ((a (atom 10)))
  (test-assert (not (atom-compare-and-set! a 1 2))))

(let ((a (atom 10)))
  (test 10 (atom-value a))
  (atom-reset! a 'hey)
  (test 'hey (atom-value a)))


(define counter (atom 0))

(define random*
  (cond-expand
    (chicken-5 pseudo-random-integer)
    (else random)))

(for-each
 thread-join!
 (map (lambda _
        (thread-start!
         (lambda ()
           (thread-sleep! (/ (random* 100) 1000))
           (atom-swap! counter + 1))))
      (iota 100)))

(test 100 (atom-value counter))


(define foo (atom (list)))

(atom-swap! foo xcons 3)
(atom-swap! foo xcons 2)
(atom-swap! foo xcons 1)

(test '(1 2 3) (atom-value foo))

(test-end)
